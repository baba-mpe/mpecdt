import numpy as np
import pylab as pl
from scipy import integrate


def normal3D(N, sigma):
    """
    Function to return a numpy array containing N iid samples from 
    a 3 dimensional normal distribution with uncorrelated components,
    mean values and variances equal to 1 and sigma^2, respectively. 
    
    inputs:
    N - number of samples
    sigma - standard deviation of components
    
    outputs:
    X - the samples
    """
    cov = sigma**2 * np.matrix(np.identity(3))
    mean = np.zeros(3)
    
    return np.random.multivariate_normal(mean, cov, N)

def Lorenz_eq(x, t, sigma, rho, beta):
    """ 
    Function to return the Lorenz equation
    Inputs:
    x - value of x
    y - value of y
    z - value of z
    sigma, rho, beta - Lorenz parameters

    Outputs:
    dxdt - value of dx/dt
    dydt - value of dy/dt
    dzdt - value of dz/dt
    """

    return [sigma * (x[1] - x[0]), x[0] * (rho - x[2]) - x[1], x[0] * x[1] - beta * x[2]]

def vectorfield(x, t, sigma, rho, beta):
    """
    Function to return the value of dx/dt, given x.
    
    Inputs:
    x - the value of x
    t - the value of t
    alpha - a parameter
    beta - another parameter

    Outputs:
    dxdt - the value of dx/dt
    """
    
    return np.array([sigma*(x[1]-x[0]),
                        x[0]*(rho-x[2])-x[1],
                        x[0]*x[1]-beta*x[2]])

def getdata(y0,T,Deltat, sigma = 10., rho =28., beta = 8./3):
    """
    Function to return simulated observational data from a dynamical
    system, for testing out forecasting tools with.

    Inputs:
    y0 - the initial condition of the system state
    T - The final time to simulate until
    Deltat - the time intervals to obtain observations from. Note
    that the numerical integration method is time-adaptive and
    chooses it's own timestep size, but will interpolate to obtain
    values at these intervals.
    sigma, rho, beta - parameters for Lorenz

    Outputs:
    data - positions of trajectories at time t
    t - array of time data
    """
    t = np.arange(0.,T,Deltat)
    data = integrate.odeint(vectorfield, y0, t=t, args=(sigma, rho, beta))
    return data, t

def plot3D(x,y,z):
    """
    Make a 3D plot of data.
    Inputs:
    x - Numpy array of x coordinate
    y - Numpy array of y coordinate
    z - Numpy array of z coordinate
    """
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x,y,z)
    pl.show()

def my_prior(sigma):
    """
    Function to draw a sample from a prior distribution used for mcmc,
    given by 3 dimensional normal distribution with uncorrelated components,
    mean value vector equal to [-0.5,-0.55, 17.] and variances equal to
    sigma^2.
    Inputs: sigma
    Outputs: the sample
    """

    cov = sigma**2 * np.matrix(np.identity(3))    
    mean = np.array([-0.5,-0.55, 17.])

    return np.random.multivariate_normal(mean, cov)

def lorenz_forward(x):
    """
    Function to return the value of the Lorenz trajectory after T=1, with
    Lorenz parameters sigma = 10., rho =28., beta = 8./3 and time step size
    Deltat =0.05.    
    Inputs: x - the initial value for Lorenz trajectory
    """

    all_vals = getdata(x,T=1,Deltat =0.05, sigma = 10., rho =28., beta = 8./3)[0]
    final_vals = all_vals[len(all_vals)-1,:]

    return final_vals

def Phi(f,sigma,x,y):
    """
    Function to return Phi, assuming a normal distribution for 
    the observation noise, with mean 0 and variance sigma.
    inputs:
    f - the function implementing the forward model
    sigma - observation noise standard deviation (components uncorrelated)
    x - a value of the model state x
    y - a value of the observed data
    """

    return np.linalg.norm(f(x)-y)**2/(2*sigma**2)

def uniform(N):
    """
    Function to return a numpy array containing N iid samples from
    U(0,1) distribution

    Inputs:
    N - number of samples

    Outputs:
    X - samples
    """
    return np.random.uniform(0,1,N)

def mcmc(f,sigma_obsErr,sigma_prior,prior_sampler,x0,yhat,N,beta):
    """
    Function to implement the MCMC pCN algorithm.
    inputs:
    f - the function implementing the forward model
    sigma_obsErr - observation noise standard deviation (components uncorrelated)
    sigma_prior - prior standard deviation (components uncorrelated)
    prior_sampler - a function that generates samples from the prior
    x0 - the initial condition for the Markov chain
    yhat - the observed data
    N - the number of samples in the chain
    beta - the pCN parameter

    outputs:
    xvals - the array of sample values
    avals - the array of acceptance probabilities
    """
    
    #set up sample array and acceptance array
    xvals = np.zeros(shape=(N+1,3))
    xvals[0] = x0
    avals = np.zeros(N)

    for i in range(N):
        
        prop = (1-beta**2)**(0.5) * xvals[i,:] + beta*prior_sampler(sigma_prior)
        

        avals[i] = min(1, np.exp( Phi(f,sigma_obsErr,xvals[i,:],yhat) \
                                - Phi(f,sigma_obsErr,prop,yhat) ))


        if avals[i] > uniform(1):
            xvals[i+1,:] = prop
        else:
            xvals[i+1,:] = xvals[i,:]
        
    return xvals,avals

#def acpt_plot(f,sigma_obsErr,sigma_prior,prior_sampler,x0,yhat,N,beta):
#    """
#    Function to draw a plot of the mcmc acceptance value evolution
#    Inputs:
#    f - the function implementing the forward model
#    sigma_obsErr - observation noise standard deviation (components uncorrelated)
#    sigma_prior - prior standard deviation (components uncorrelated)
#    prior_sampler - a function that generates samples from the prior
#    x0 - the initial condition for the Markov chain
#    yhat - the observed data
#    N - the number of samples in the chain
#    beta - the pCN parameter
#    """
#    
#    vals = np.cumsum(mcmc(f,sigma_obsErr,sigma_prior,prior_sampler,x0, \
#                            yhat,N,beta)[1])/np.arange(1,N+1)
#
#    pl.plot(np.arange(1,N+1), vals)
#    pl.xlabel('Sample Size')
#    pl.ylabel('Acceptance Rate')
#    pl.legend(loc='best', fontsize=14)

if __name__ == '__main__': 

    # Set up initial data, parameters for MCMC
    sample_size = 5000
    beta = 0.01
    sigma_obsErr = 0.1
    sigma_prior = 0.01
    init = [-0.587,-0.563,16.870]
    mcmc_startVal = [-0.2,-0.4,10]
    mydata, mytime = getdata(init, T = 1., Deltat = 0.05)

    # Plot Lorenz trajectory, calculate observation 
    plot3D(mydata  [:,0],mydata[:,1],mydata[:,2])
    observals = mydata[len(mydata)-1,:] + normal3D(1,sigma_obsErr)

    # Compute MCMC samples 
    mcmc_vals = mcmc(lorenz_forward,sigma_obsErr,sigma_prior,my_prior,mcmc_startVal, \
                     observals,sample_size,beta)

    #Plot acceptance rate as a function of sample size
    acc_vals = np.cumsum(mcmc_vals[1])/np.arange(1,sample_size+1)
    pl.plot(np.arange(1,sample_size+1), acc_vals)
    pl.xlabel('Sample Size')
    pl.ylabel('Acceptance Rate')
    pl.legend(loc='best', fontsize=14)    
    pl.show()

    # Calculate MCMC samples used for determining posterior pdf 
    burnIn_val = float(input("Please enter number of neglected samples from start: ")) 

    if burnIn_val < sample_size:
        used_vals = mcmc_vals[0][burnIn_val:]
    else:
        raise NameError("Number of neglected samples must be smaller than sample size.")

    # Calculate means, variances and covariance matrix of posterior pdf
    mcmc_means = [np.mean(used_vals[:,0]), np.mean(used_vals[:,1]), np.mean(used_vals[:,2])]
    mcmc_vars = [np.var(used_vals[:,0]), np.var(used_vals[:,1]), np.var(used_vals[:,2])]
    cov_matrix = np.cov(used_vals.T)

    print("MCMC gives posterior means =", mcmc_means)
    print("MCMC gives posterior variances =", mcmc_vars)
    print("MCMC gives posterior covariance matrix = ", cov_matrix)

    # Plot posterior PDF histograms based on one observation 
    fig = pl.figure()
    fig.suptitle('Posterior PDF Histograms based on one Observation', fontsize=14)

    pl.subplot(311)
    pl.hist(used_vals[:,0], 50, normed = 1, label = "Posterior PDF Histogram in x-direction")
    pl.legend(loc='best', fontsize=14)

    pl.subplot(312)
    pl.hist(used_vals[:,1], 50, normed = 1, label = "Posterior PDF Histogram in y-direction")
    pl.legend(loc='best', fontsize=14)

    pl.subplot(313)
    pl.hist(used_vals[:,2], 50, normed = 1, label = "Posterior PDF Histogram in z-direction")
    pl.legend(loc='best', fontsize=14)
    pl.show()
    
