import numpy as np
import pylab as pl
import numpy as np

def lmap(x):
    """
    Function to evaluate y = a*x + b for a = 2, b = 1

    inputs:
    x - a numpy array of values

    outputs:
    y - a numpy array containing the double well polynomial evaluated on x
    """
    a = 1
    b = 0
    return a*x + b

def myprior():
    """
    Function to draw a sample from a prior distribution given by the
    normal distribution with variance alpha.
    inputs: none
    outputs: the sample
    """
    alpha = 1.0
    return alpha**0.5*np.random.randn()

def Phi(f,x,y):
    """
    Function to return Phi, assuming a normal distribution for 
    the observation noise, with mean 0 and variance sigma.
    inputs:
    f - the function implementing the forward model
    x - a value of the model state x
    y - a value of the observed data
    """
    sigma = 1
    return (f(x)-y)**2/(2*sigma**2)

def uniform(N):
    """
    Function to return a numpy array containing N iid samples from
    U(0,1) distribution

    Inputs:
    N - number of samples

    Outputs:
    X - samples
    """
    return np.random.uniform(0,1,N)

def mcmc(f,prior_sampler,x0,yhat,N,beta):
    """
    Function to implement the MCMC pCN algorithm.
    inputs:
    f - the function implementing the forward model
    prior_sampler - a function that generates samples from the prior
    x0 - the initial condition for the Markov chain
    yhat - the observed data
    N - the number of samples in the chain
    beta - the pCN parameter

    outputs:
    xvals - the array of sample values
    avals - the array of acceptance probabilities
    """
    xvals = np.zeros(N+1)
    xvals[0] = x0
    avals = np.zeros(N)

    for i in range(N):
        
        prop = (1-beta**2)**(0.5) * xvals[i] + beta*myprior()
        

        avals[i] = min(1, np.exp( Phi(f,xvals[i],yhat) - Phi(f,prop,yhat) ))


        if avals[i] > uniform(1):
            xvals[i+1] = prop
        else:
            xvals[i+1] = xvals[i]
        

    return xvals,avals

def acpt_plot(f,prior_sampler,x0,yhat,N,beta):
    
    vals = np.cumsum(mcmc(f,prior_sampler,x0,yhat,N,beta)[1])/np.arange(1,N+1)

    pl.plot(np.arange(1,N+1), vals)


if __name__ == '__main__':    
    yhat = 1.
    x0 = 0.
    "xvals,avals = mcmc(lmap,myprior,x0,yhat,10000,0.17)"

    acpt_plot(lmap,myprior,x0,yhat,5000,0.9)
    pl.show()

    burnIn_val = float(input("Please enter number of neglected samples from start: "))
    used_vals = mcmc(lmap,myprior,x0,yhat,10000,0.9)[0][burnIn_val:]

    mcmc_mean = np.mean(used_vals)
    mcmc_var = np.var(used_vals)
    print("MCMC gives mean =", mcmc_mean)
    print("MCMC gives variance =", mcmc_var)

    pl.hist(used_vals, 100, normed = 1, label = "Posterior PDF Histogram")
    pl.plot(np.linspace(np.min(used_vals),np.max(used_vals),500), \
            pl.normpdf(np.linspace(np.min(used_vals),np.max(used_vals),500), 0.5, np.sqrt(0.5)), \
            label = 'Posterior PDF')
    pl.legend(loc='best', fontsize=14)
    pl.show()




