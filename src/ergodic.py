import numpy as np
import pylab as pl
from scipy import integrate
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib


def vectorfield(x, t, sigma, rho, beta):
    """
    Function to return the value of dx/dt, given x.
    
    Inputs:
    x - the value of x
    t - the value of t
    alpha - a parameter
    beta - another parameter

    Outputs:
    dxdt - the value of dx/dt
    """
    
    return np.array([sigma*(x[1]-x[0]),
                        x[0]*(rho-x[2])-x[1],
                        x[0]*x[1]-beta*x[2]])

def ensemble_plot(Ens,title=''):
    """
    Function to plot the locations of an ensemble of points.
    """

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(Ens[:,0],Ens[:,1],Ens[:,2],'.')    
    pylab.title(title)

def ensemble(M=100,T=1,stDev=0.1):
    """
    Function to integrate an ensemble of    trajectories of the Lorenz system.
    Inputs:
    M - the number of ensemble members
    T - the time to integrate for
    """
    ens = stDev * np.random.randn(M,3)

    for m in range(M):
        t = np.array([0.,T])
        data = integrate.odeint(vectorfield, 
                                y0=ens[m,:], t=t, args=(10.0,28.,8./3.),
                                mxstep=100000)

        ens[m,:] = data[1,:]

    return ens

def f(d):
    """
    Function to apply ergodic average to.
    inputs:
    X - array, first dimension time, second dimension component
    outputs:
    f: - 1d- array of f values
    """

    return d[:,0]**2 + d[:,1]**2 + d[:,2]**2

def f_2(d):
    """
    Function to apply ergodic average to.
    inputs:
    X - array, first dimension time, second dimension component
    outputs:
    f_2: - 1d-
    """
    
    return d[:,0] + d[:,1] + d[:,2]

def f_3(d):
    """
    Function to apply ergodic average to.
    inputs:
    X - array, first dimension time, second dimension component
    outputs:
    f_3: - 1d-
    """    
    return np.exp(d[:,0]) + np.exp(d[:,1]) + np.exp(d[:,2])

def space_av(f, M, T, stDev):
    """ 
    Computes spatial average of f evaluated at ensemble members
    Inputs:
    f - funtion to apply spatial average to
    M - number of ensemble members
    T - time at which ensemble is created
    """
    
    Ens = ensemble(M, T, stDev)
    
    return np.average( f(Ens) )
    
def space_var(f, M, T, stDev, N):
    """
    Computes variance of space averages
    Inputs:
    f, M, T - see function space_av
    N - number of Monte Carlo procedures applied to f and ensemble members
    stDev - standart deviation for initial condition 
    """
    #set up range of space averages whose variance will be computed
    space_avs = np.zeros(N)
    for i in range(N):
           space_avs[i] = space_av(f, M, T, stDev)   

    return np.var(space_avs)

def space_varPlot(f, M, T, stDev_range, N):
    """
    plots variance of space averages as function of variance of inital condition
    Inputs:
    f, M, T - see function space_av
    N - see function space_var
    stDev_range - range of standard deviations of initial condition
    """
    #set up range of space variances used for plot
    space_vars = np.zeros(np.size(stDev_range))

    for k in range(np.size(stDev_range)):
        space_vars[k] = space_var(f, M, T, stDev_range[k], N)
 
    pl.plot(stDev_range, space_vars,    'b')
    pl.xlabel(r"$\mathdefault{stDev}$")
    pl.ylabel(r"$\mathdefault{\operatorname{Var}(TimeAverages)}$")
    pl.title("Variance of space average as function of standard deviation \
of initial condition")

def getdata(y0,T,Deltat, sigma = 10., rho =28., beta = 8./3):
    """
    Function to return simulated observational data from a dynamical
    system, for testing out forecasting tools with.

    Inputs:
    y0 - the initial condition of the system state
    T - The final time to simulate until
    Deltat - the time intervals to obtain observations from. Note
    that the numerical integration method is time-adaptive and
    chooses it's own timestep size, but will interpolate to obtain
    values at these intervals.
    sigma, rho, beta - parameters for Lorenz

    Outputs:
    data - positions of trajectories at time t
    t - array of time data
    """
    t = np.arange(0.,T,Deltat)
    data = integrate.odeint(vectorfield, y0, t=t, args=(sigma, rho, beta))
    return data, t


def time_av(y0, f, N, Deltat, sigma = 10., rho =28., beta = 8./3):
    """
    calculates time average of function f
    Inputs:
    f - function to apply time average over
    y0 - initial conditions of the system state
    N - number of time steps used for averaging
    Deltat - size of time step
    sigma, rho, beta - parameters for lorenz equation

    Outputs:
    time_av = time average of function f
    """
           
    mydat, mytime = getdata(y0, N*Deltat, Deltat, sigma, rho, beta)
        
    return np.average(f(mydat))

def time_avPlot(y0, f, N_range, Deltat):
    """
    Plots the time average of f as function of number of time steps
    Inputs:
    y0, f, Deltat - see function time_av
    N_range - range of numbers of time steps used for averaging
    """
    #set up range of time averages used for plot   
    time_avs = np.zeros(len(N_range))
 
    for N in range(len(N_range)):
        time_avs[N] = time_av(y0, f, N_range[N], Deltat)

    pl.plot(N_range, time_avs)
    pl.xlabel(r"$\mathdefault{N}$")
    pl.ylabel(r"$\mathdefault{\frac{1}{N}\sum_{i=1}^{N}f(X^i)}$")
    pl.title('Time Average as function of time step number')


def prmtPlot(rho_range, sigma_range, beta_range):
    """
    plots trajectory of a particle for range of parameter values
    Inputs:
    rho,sigma,beta_range - range of 6 values taken by varying parameter     
    """
    #set up plot visualisation 
    subplot_list=[231,232,233,234,235,236] #arguments for subplot
 
    #fig 1: changes in rho   
    fig1 = pl.figure()
    for i, rho  in enumerate(rho_range):   
        mydat, mytime = getdata([-0.587,-0.563,16.870],500,0.1,rho=rho)
        ax = fig1.add_subplot((subplot_list[i]), projection='3d')
        ax.plot(mydat[:,0],mydat[:,1],mydat[:,2], 'k-')
        pl.title(r"$\rho$ = %f" % rho)
    
    #fig 2: changes in sigma
    fig2 = pl.figure()
    for i,sigma in enumerate(sigma_range):
        mydat, mytime = getdata([-0.587,-0.563,16.870], 500,0.1,sigma=sigma)
        ax = fig2.add_subplot((subplot_list[i]), projection='3d')
        ax.plot(mydat[:,0],mydat[:,1],mydat[:,2], 'k-')
        pl.title('$\sigma$ = %f' % sigma)
    
    #fig 3: changes in beta
    fig3 = pl.figure()
    for i,beta in enumerate(beta_range):
        mydat, mytime = getdata([-0.587,-0.563,16.870],500,0.1,beta=beta)
        ax = fig3.add_subplot((subplot_list[i]), projection='3d')
        ax.plot(mydat[:,0],mydat[:,1],mydat[:,2], 'k-')
        pl.title(r"$\beta$ = %f" % beta)


def parTime_avPlot(y0, f, N, Deltat, prmt_no, prmt_range):
    """
    plots time average of f as function of Lorenz parameters
    Inputs:
    y0, f, N, Deltat - see function time_av
    prmt_no - number identifying varying parameter. if 1: rho, if 2: sigma, if 3: beta
    prmt_range - range of Lorenz parameter
    """
    #set up range of time averages used for plot
    time_avs = np.zeros(len(prmt_range))
    pl.figure()

    #case 1: changes in rho
    if prmt_no == 1:
        for i in range(len(prmt_range)):
            time_avs[i] = time_av(y0, f, N, Deltat, rho=prmt_range[i])
        pl.xlabel(r"$\mathdefault{\rho}$")
    #case 2: changes in sigma
    elif prmt_no == 2:
        for i in range(len(prmt_range)):
            time_avs[i] = time_av(y0, f, N, Deltat, sigma=prmt_range[i])
        pl.xlabel(r"$\mathdefault{\sigma}$")
    #case 3: changes in beta
    elif prmt_no == 3:
        for i in range(len(prmt_range)):
            time_avs[i] = time_av(y0, f, N, Deltat, beta=prmt_range[i])
        pl.xlabel(r"$\mathdefault{\beta}$")
    else:
        print 'Choose prmt_no as 1,2 or 3.'


    pl.plot(prmt_range, time_avs)
    pl.ylabel(r"$\mathdefault{\frac{1}{N}\sum_{i=1}^{N}f(X^i)}$")
    pl.title("Time Average as function of Lorenz parameter")

    
def pardif_prmt(y0, f, N, Deltat, prmt_no, epsilon=0.01):
    """
    Computes difference quotient of time averaged f wrt one Lorenz parameter
    Inputs:
    y0, f, N, Deltat, prmt_no - see function parTime_avPlot
    epsilon - value of Lorenz parameter change used for difference quotient
    """
    f_0 = time_av(y0, f, N, Deltat)

    #same cases as in function parTime_avPlot
    if prmt_no == 1:
        f_eps = time_av(y0, f, N, Deltat, rho = 28. + epsilon)
    elif prmt_no == 2:
        f_eps = time_av(y0, f, N, Deltat, sigma = 10. + epsilon)
    elif prmt_no == 3:
        f_eps = time_av(y0, f, N, Deltat, beta = 8./3 + epsilon)
    else:
        print 'Choose prmt_no as 1,2 or 3.'
    return (f_eps - f_0)/epsilon


def pardif_plot(y0, f, N, Deltat, prmt_no, epsilon_range):
    """
    Plots difference quotient of time averaged f wrt one Lorenz parameter as
    function of step size for difference quotient
    Inputs:
    y0, f, N, Delta, prmt_no - see function pardif_prmt
    epsilon_range - range of step sizes for difference quotient
    """
    pl.figure()
    pl.xlabel(r'$\mathdefault{\epsilon}$')

    #set up range of diff quotient values used for plot
    pardif_f = np.zeros(len(epsilon_range))  
    for i in range(len(epsilon_range)):         
        pardif_f[i] = pardif_prmt(y0, f, N, Deltat, prmt_no, epsilon_range[i])
    if prmt_no == 1:
        pl.ylabel(r"$ \mathdefault{\frac{\overline{f}(\rho + \epsilon, \sigma, \beta)-\overline{f}(\rho, \sigma, \beta)}{\epsilon}}$")
    elif prmt_no == 2:
        pl.ylabel(r"$\mathdefault{\frac{\overline{f}(\rho, \sigma + \epsilon, \beta)-\overline{f}(\rho, \sigma, \beta)}{\epsilon}}$")
    elif prmt_no == 3:
        pl.ylabel(r"$\mathdefault{\frac{\overline{f}(\rho, \sigma, \beta+ \epsilon)-\overline{f}(\rho, \sigma, \beta)}{\epsilon}}$")

    pl.plot(epsilon_range, pardif_f)


if __name__ == '__main__':


# Exercise 2 - Part 1 #####
# Plot of the variance of time averages as function of standard deviation of initial condition

    space_varPlot(f, 100, 1, np.arange(0.1,2.5,.1), 20)

    print "Exercise 2 - Part 1: It can be seen that starting with a small variance of the initial conditions, the \
variance of the time averages decreases by increasing the variance of the initial conditions. \
Heuristically, this means, the less deterministic we choose our initial conditions, the more \
deterministic are our time averages. This might seem counter-intuitive at first glance, but \
makes sense by considering the fact that our system evolves chaotically, that is, even small \
changes in initial conditions result in a wide spread of the system's evolution. Meanwhile, \
higher variance of the initial conditions could mean that the attractor of the system is better \
covered by the evolution of the ensemble members over time such that the time average converges \
faster to the ergodic limit. On the other hand \
side, the variance of the time averages lateron increases when the variance of the initial \
condition is increased over a certain level (around 2.5). This could have something to do \
with the possibility of having many ensemble values outside the attractor before entering it. \
Note that the invariant measure is supported on the attractor."
    pl.show()  

####################### Exercise 2 - Part 2 #####
# Plot of the time average of f as function of number of used time steps

    time_avPlot(np.random.randn(3), f, np.arange(100,600,1), 0.1)
     
    print ("Exercise 2 - Part 2: The plot shows that the time average converges from above towards the time average \
limit value. The convergence rate looks somehow slower than 1/sqrt(N). In this example, the \
value of time average does not change any more significantly after N=1000.")
    pl.show()  

####################### Exercise 2 - Part 3 #####
# Plot of the time average of f as function of number of used time steps
# for 5 different initial conditions

    pl.figure()
    for x in range(5): 
        time_avPlot(np.random.randn(3), f, np.arange(1,500,1), 0.1)
        pl.xlabel('Number of time steps')
        pl.ylabel('Time average')
        pl.title('Time average for different initial conditions')

    print ("Exercise 2 - Part 3: Every line in the plot reflects the time average evolution based on a different \
initial condition. Obviously, all of them seem to converge towards the same value as the \
number of time steps grows large. Therefore it can be infered that the time average indeed \
does not depend on the initial conditions.")
    pl.show()

##################### Exercise 3 - Part 1 #####
# Visualisation of the support of the invariant measure by plotting trajectory of a particle \
# for a range of Lorenz parameter values.

# Different parameter ranges
    rho_range = [28.-1.,28.-0.5,28.,28.+0.5,28.+1.,28.+1.5]
    sigma_range = [10.-1.,10.-.5,10.,10.+.5,10.+1.,10.+1.5]
    beta_range = [8./3-1., 8./3-.5, 8./3, 8./3+0.5, 8./3+1., 8./3+1.5]
# Plots the results
    prmtPlot(rho_range,sigma_range,beta_range)

    print("Exercise 3 - Part 1: The trajectories depending on the Lorenz parameters have the following \
characteristics: By small changes of the parameters rho and sigma, the trajectory \
and therefore the attractor does not change in shape too much. Yet, increasing beta \
results in a thinning out of the left hand side of the attractor and an all-over \
allocation of points on the right hand side of the attractor. For the highest value of beta \
both attractor parts are thinned out where the left hand side only consists of few \
lines of trajectory. One could argument that it seems that in this case of parameter \
choice the trajectory is less chaotic.")
    pl.show()  

##################### Exercise 3 - Part 2 #####
# Plot of the time average of f as function of Lorenz parameters. Investigation of parameter
# towards which the time average is most sensitive to.

# Plot as function of rho   
    parTime_avPlot(np.random.randn(3), f, 500, 0.1, 1, np.linspace(27.,29.,25))
# Plot as function of sigma
    parTime_avPlot(np.random.randn(3), f, 500, 0.1, 2, np.linspace(9.,11.,25))
# Plot as function of beta
    parTime_avPlot(np.random.randn(3), f, 500, 0.1, 3, np.linspace(1.7,3.7,25))

    print ("Exercise 3 - Part 2: The plots show that the time average is most sensitive towards changes in rho, and the \
the least sensitive in sigma among all Lorenz parameters. Note that the scales of the ranges \
of the parameters are all of a comparable size. The response is mostly linear for the choice \
of range of rho (from around 25 to 31, so rho_prime = 3), but not linear for beta and sigma. \
Considered on a lower scale, the response for beta seems to have two regions for linear \
response where in between these regions there is sharp upward bend (between 3 and 4).")

    pl.show()

# Plots of the time averages of f_2, f_3 as function of rho
    parTime_avPlot(np.random.randn(3), f_2, 500, 0.1, 1, np.linspace(27.,28.,25))
    parTime_avPlot(np.random.randn(3), f_3, 500, 0.1, 1, np.linspace(27.9,28.1,25))

    print ("We receive a non linear response for rho by using the linear function f_2 and the exponential \
function f_3 within the scales that we used.")

    pl.show()

########################## Exercise 3 - Part 3 #####
# Plot of the difference quotient of time averaged f with respect to one Lorenz parameter as
# a function of step size for difference quotient. Investigating differentiability of the time
# average with respect to the different Lorenz parameters.

    pardif_plot(np.random.randn(3), f, 500, 0.1, 1, np.arange(-.01,.01,0.0001))
    pardif_plot(np.random.randn(3), f, 500, 0.1, 2, np.arange(-.01,.01,0.0001))
    pardif_plot(np.random.randn(3), f, 500, 0.1, 3, np.arange(-1,1,0.01))  

    print ("Exercise 3 - Part 3: For small absolute values of Epsilon all three differential quotients oscillate. \
For decreasing absolute values of Epsilon the differential quotien with respect to beta diverges. \
This means that the time average is non differentiable in beta. The differential quotient with \
respect to rho and sigma does not diverge in an obvious way on the scales used for changes in beta. \
However, by choosing smaller scales we see that the difference quotient with respect to \
rho and sigma also seems to diverge. Obviously, from a purely non analytical view this is hard to \
prove. By taking larger values of Epsilon, one can see that in average the differential quotient \
with respect to sigma and beta tends to zero \
where the one with respect to rho is equal to a value larger than zero (around 50) representing \
the slope of the linear response in rho.")

    pl.show()

    pardif_plot(np.random.randn(3), f_2, 500, 0.1, 1, np.arange(-.01,.01,0.0001))
    pardif_plot(np.random.randn(3), f_3, 500, 0.1, 2, np.arange(-.01,.01,0.0001))
 
    print ("We receive same results if we change our test function into a linear one (f_2) or into a \
esponential one (f_3) where in the latter case the divergences are more obvious. ") 

    pl.show()  


