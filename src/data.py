from scipy import integrate
import numpy as np
import pylab
import math

"""
File containing code relating to Computational Lab 1 of the Dynamics
MPECDT core course.

You should fork this repository and add any extra functions and
classes to this module.
"""

def Lorenz_eq(x, t, sigma, rho, beta):
    """bla
    
    Inputs:
    x - value of x
    y - value of y
    z - value of z
    
    Outputs:
    dxdt - value of dx/dt
    dydt - value of dy/dt
    dzdt - value of dz/dt
    """

    return [sigma * (x[1] - x[0]), x[0] * (rho - x[2]) - x[1], x[0] * x[1] - beta * x[2]]


def vectorfield(x, t, alpha, beta):
    """
    Function to return the value of dx/dt, given x.
    
    Inputs:
    x - the value of x
    t - the value of t
    args - an object containing parameters

    Outputs:
    dxdt - the value of dx/dt
    """

    return alpha*x + beta

def getdata(y0,T,Deltat):
    """
    Function to return simulated observational data from a dynamical
    system, for testing out forecasting tools with.

    Inputs:
    y0 - the initial condition of the system state
    T - The final time to simulate until
    Deltat - the time intervals to obtain observations from. Note
    that the numerical integration method is time-adaptive and
    chooses it's own timestep size, but will interpolate to obtain
    values at these intervals.
    """
    t = np.arange(0.,100,0.05)
    data = integrate.odeint(Lorenz_eq, y0, t=t, args=(10.0, 28.0, 8.0/3))
    return data, t

def plot3D(x,y,z):
    """
    Make a 3D plot of data.
    Inputs:
    x - Numpy array of x coordinate
    y - Numpy array of y coordinate
    z - Numpy array of z coordinate
    """
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x,y,z)
    pylab.show()

def predict_linear(mydata, mytime):
    """
    Gives 1-step linear approximation of solution to Lorenz_eq.
    Input:
    mydata - array of solution to Lorenz_eq
    mytime - time discretization array
    The next value of the time series is given by the linear 
    extrapolation of the previous two (exact) observations.
    """
    pred_data = 0.0 * mydata

    for n,t in enumerate(mytime):
        if n<2:
            pred_data[n,:] = mydata[n,:]
        else:
            pred_data[n,:] = 2 * mydata[n-1,:] - mydata[n-2,:]
    return pred_data

def rmse(mydata, pred_data, number_approx):
    """
    Computes the root-mean-square error (rmse) of the linearly 
    extrapolated solution to Lorenz_eq.
    Inputs:
    mydata - array of solution to Lorenz_eq
    pred_data - array of linearly extrapolated solution to Lorenz_eq
    number_approx - no. of approximation values involved to compute rmse
    """
    sq_diff = 0.0 * mydata

    for n in np.arange(0, number_approx, 1):
        sq_diff[n,:] = (mydata[n,:] - pred_data[n,:]) ** 2

    return math.sqrt(1 / number_approx * sum(sq_diff[:,0])), math.sqrt(1 / number_approx * sum(sq_diff[:,1])), math.sqrt(1 / number_approx * sum(sq_diff[:,2]))

   
def rmse_elegant(mydata, pred_data, number_approx):
    """
    Does the same thing as rmse(), but without using a loop.
    Note that python capable of applying functions of arrays! 
    Uses componentwise subtraction of arrays and trimming a arrays
    """
    diff_data = mydata - pred_data
    diff_data_trim = diff_data[:number_approx]

    return math.sqrt(1 / number_approx * sum((abs(diff_data_trim[:,0])) ** 2)), math.sqrt(1 / number_approx * sum((abs(diff_data_trim[:,1])) ** 2)), math.sqrt( 1/ number_approx * sum((abs(diff_data_trim[:,2])) ** 2))

def predict_linear_nstep(mydata, mytime, step):
    """""
    Gives n-step linear approximation of solution to Lorenz_eq.
    Input:
    mydata - array of solution to Lorenz_eq
    mytime - time discretization array
    step - number of steps ahead to make the forecast.
    The next value of the time series is given by the linear 
    extrapolation of the two observations lying n, n-1 steps in the past.
    """
    pred_data_nstep = 0.0 * mydata
    
    for n,t in enumerate(mytime):
        if n<step+1:
            pred_data_nstep[n,:] = mydata[n,:]
        else:
            pred_data_nstep[n,:] = (step + 1.0) * mydata[n-step,:] - ( (step - 1.0) * 3.0 - 1.0) * mydata[n-step-1.0,:]
    return pred_data_nstep

if __name__ == '__main__':
    mydata, mytime = getdata([-0.587,-0.563,16.87],100,0.05)
    "plot3D(mydata[:,0],mydata[:,1],mydata[:,2])"

    """pred_data = predict_linear(mydata,mytime)
    plot3D(pred_data[:,0], pred_data[:,1], pred_data[:,2])"""

    pred_data_nstep = predict_linear_nstep(mydata, mytime, 2.0)
    plot3D(pred_data_nstep[:,0], pred_data_nstep[:,1], pred_data_nstep[:,2])

    """pylab.plot(mytime, mydata[:,1])
    pylab.plot(mytime, pred_data[:,1])
    pylab.show()"""

    """print rmse_elegant(mydata, pred_data, 2000.0)
    print rmse(mydata, pred_data, 2000.0)"""
   
    
   

    




