import pylab as pl
import numpy as np
from scipy.integrate import quad
import matplotlib.font_manager as font_manager

def V_bistable(x):
    """
    Function to compute the function value of the potential V_bistable of
    the input. If input is numpy array, then also the returned values.
    inputs:
    x - value or numpy array of values

    outputs:
    y - value or numpy array containing function values of the derivative
    function of V_bistable of the input values
    """
    return x**4/4. - x**2/2.

def Vprime_bistable(x):
    """
    Function to compute the function value of the derivative of the potential 
    V_bistable of the input. If input is numpy array, then also the returned 
    values.
    inputs:
    x - value or numpy array of values

    outputs:
    y - value or numpy array containing function values of the derivative
    function of V_bistable of the input values
    """
    return x**3-x

def idendity(x):
    """
    Function to compute the idendity of the input. If input is numpy array, 
    then also the returned values.
    inputs:
    x - value or numpy array of values

    outputs:
    y - value or numpy array containing all of the input values
    """
    return x

def square(x):
    """
    Function to compute the square of the input. If input is numpy array, 
    then also the returned values.
    inputs:
    x - value or numpy array of values

    outputs:
    y - value or numpy array containing the square of all of the input values
    """
    return x**2

def brownian_dynamics(x,Vp,sigma,dt,T,tdump):
    """
    Solve the Brownian dynamics equation

    dx = -V(x)dt + sigma*dW

    using the Euler-Maruyama method

    x^{n+1} = x^n - V(x^n)dt + sigma*dW

    inputs:
    x - initial condition for x
    Vp - a function that returns V'(x) given x
    sigma - parameter in Fokker-Planck / SDE
    dt - the time stepsize
    T - the time limit for the simulation
    tdump - the time interval between storing values of x

    outputs:
    xvals - a numpy array containing the values of x at the chosen time points
    tvals - a numpy array containing the values of t at the same points
    """

    # set up initial values
    xvals = [x]
    t = 0.
    tvals = [0]
    dumpt = 0.
    
    # apply Euler-Maruyama method
    while(t<T-0.5*dt):
        dW = dt**0.5*np.random.randn()
        x += -Vp(x)*dt + sigma*dW

        t += dt
        dumpt += dt

        if(dumpt>tdump-0.5*dt):
            dumpt -= tdump
            xvals.append(x)
            tvals.append(t)
    return np.array(xvals), np.array(tvals)

def stat_pdf(x, sigma, V = V_bistable):
    """  
    Computes stationary probability density function corresponding to Fokker-
    Planck equation, which exists and is unique in case that V=V_bistable, by
    calculating mass constant C.

    Inputs:
    x - value or numpy array of values

    Outputs:
    y - value or numpy array containing one over square root of all input
    values   
    """

    norm_integrand = lambda x: np.exp(-2/(sigma**2) * V(x))
    norm = quad(norm_integrand, -np.inf,np.inf)

    return 1/norm[0] * np.exp(-2./(sigma**2) * V(x))

def ensembleHist(x, Vp, V, sigma, dt, T_range, tdump, N):
    """
    Creates histograms of relative frequency of solutions of Brownian
    dynamics equation at different points of time and plots stationary
    probability density function in same figure

    inputs:
    x - initial condition for x
    Vp - a function that returns V'(x) given x
    V - function giving the potential
    sigma - parameter in Fokker-Planck / SDE
    dt - the time stepsize
    T_range - range of time values for which histograms are created
    tdump - the time interval between storing values of x
    N - number of sample size used for creating every histogram
    """

    T = T_range[len(T_range) -1]
    
    # set up ranges of solutions to Brownian dynamics used for plots
    vals = np.zeros((N,5))

    for i in range(N):
        brown_dyn, t = brownian_dynamics(x,Vp,sigma,dt,T,tdump)
        vals[i,:] = brown_dyn[(T_range/dt).astype(int)]
    
    #set up plot visualisation
    subplot_list=[511,512,513,514,515]

    fig1 = pl.figure()
    for i  in range(len(T_range)):   
        ax = fig1.add_subplot((subplot_list[i]))
        
        ax.hist(vals[:,i], 100, normed = 1, \
                label = "Ensemble, T = %f" % T_range[i])
        pl.plot(np.linspace(np.min(vals),np.max(vals),500), \
            stat_pdf(np.linspace(np.min(vals),np.max(vals),500),sigma,V), \
            label = 'Stationary pdf')
        pl.legend(loc='best', fontsize=10)  
                  

def trueExpec(f, sigma, V = V_bistable):
    """
    Function to compute E[f(X)] based on integrating f multiplied by 
    stationary probability density function. To be considered as true 
    or reference E[f(X)]. 

    Inputs:
    f - function over which time average will be considered
    sigma - parameter in Fokker-Planck / SDE
    V - function of the potential
    """

    integrand = lambda x: f(x)*stat_pdf(x, sigma, V)
    return quad(integrand, -np.inf, np.inf)    

def timeAverage(x,Vp,sigma,dt,N_range,tdump,f):
    """
    Function to calculate the time average of function f
    Inputs:
    x - initial condition for x
    Vp - a function that returns V'(x) given x, see brownian_dynamics
    sigma - parameter in Fokker-Planck / SDE
    dt - the time stepsize
    N_range - range of time steps for which time average is computed
    tdump - time interval between storing values of x, see brownian_dynamics  
    f - function to apply time average for

    Outputs:
    vals = time averages of function f computed over N_range
    """

    N = N_range[len(N_range)-1]
    T = N * dt   

    brown_dyn = brownian_dynamics(x,Vp,sigma,dt,T,tdump)[0]
    vals = (np.cumsum(f(brown_dyn),dtype=float)\
            /np.arange(1,brown_dyn.size+1))[N_range]

    return vals


def timeAV_errorPlot(x,V,Vp,sigma,dt,N_range,tdump,f):
    """
    Plots the error of the time average of f as function of number of 
    time steps

    Inputs:
    x - initial condition for x
    V - function of the potential, used to compute true expectation
    Vp - a function that returns V'(x) given x, see function timeAverage
    sigma - parameter in Fokker-Planck / SDE
    dt - the time stepsize
    N_range - range of time steps for which time average is computed
    tdump - time interval between storing values of x, see fct timeAverage  
    f - function to apply time average for
    """
    averages = timeAverage(x,Vp,sigma,dt,N_range,tdump,f)
    expec = trueExpec(f, sigma, V)[0]
    vals = np.absolute(averages - expec)

    # choose log-scale for y-axis
    pl.gca().set_yscale('log')

    pl.plot(N_range, vals)
    pl.legend(loc = "best")
    
if __name__ == '__main__':

###### Exercise 1 ############################################

#    x,t = brownian_dynamics(0.0,Vprime_bistable,0.5,0.1,500,0.1)
#    pl.plot(t,x)

#    pl.show()

###### Exercise 2 ############################################

    # plots histograms and invariant solution
    ensembleHist(1.0,Vprime_bistable,V_bistable,0.5,0.1, \
                np.array([0.5,2.5,10,50,250]),0.1, 1000)

    print ("Exercise 2: Each histogram can be considered as an approximative \
solution of the Fokker-Planck equation at the corresponding point of time T in \
the following sense: As known, the probability density function corresponding \
to the solution X(T) of the stochastic differential equation from (1) on the \
exercise sheet evolves according to the Fokker-Planck equation for T>0. A \
histogram normalized by 1 at time T states the relative frequency of the \
realizations of X(T). Therefore, the histogram represents an approximation \
for the probability density function for X(T), thus an approximation of the \
solution of Fokker-Planck. For small values of T, the histograms have most \
weight on the right hand side of the plot around the initial value X(0)=1, \
position of one of the two minima of the potential. This makes sense as within \
a small time interval, a transition over the potential height between the \
two sinks of the potential is not likely to happen. By increasing T, the \
histograms become more balanced \
between the two sinks that lie symmetric to zero, and closer to the shape of the \
stationary probability density function. \
Note that the time step size dt was chosen to be =0.1 for the Euler-Murayama \
method used for creating the samples for these histograms, and that the number \
of samples in each histogram equals to 1000. The last and penultimate histograms \
(T=50, 250) are very close to the analytical steady-state density function. This \
can be considered as a fast convergence comparing to previous results on \
convergences from exercises. Further increasing T does not increase the \
closeness of the histograms to the stationary solution given the sample \
number and discretization of the histogram plots. ")
    
    pl.show()

###### Exercise 3 ###########################################

###### Part 1

    # set up range of time steps for which plot is created
    N_range1 = np.linspace(10**1,10**6,1000).astype(int)

    # set up plot visualisation
    subplot_list=[211,212] 
    fig1 = pl.figure()

    # plots time averages for f(x)=x
    timeAV0 = fig1.add_subplot((subplot_list[0]))
    timeAV0.plot(N_range1, timeAverage(1.0,Vprime_bistable,0.5,0.1, \
                                    N_range1,0.1, idendity))
    pl.xlabel("Time step number N")
    pl.ylabel(r"$\mathdefault{\frac{1}{N}\sum_{i=1}^{N}f(X(t_i))}$")
    pl.title("Time averages for $f(x)=x$ (above) and $f(x)=x^2$ (below)")

    # plots time averages for f(x)=x^2
    timeAV1 = fig1.add_subplot((subplot_list[1]))
    timeAV1.plot(N_range1, timeAverage(1.0,Vprime_bistable,0.5,0.1, \
                                    N_range1,0.1, square))
    pl.xlabel("Time step number N")
    pl.ylabel(r"$\mathdefault{\frac{1}{N}\sum_{i=1}^{N}f(X(t_i))}$")
   
    print ("Exercise 3 Part 1: The plot shows the evolution of the time averages \
for the two considered functions as a function of the number of time steps N. One \
can easily see that the time average is converging for increasing N for both functions, \
more precisely the value of the time averages does not change significantly any more \
when N is large on this linearly scaled plot. Note that the time step size dt again \
was chosen to be =0.1.")
    pl.show()

###### Part 2

    # set up range of time steps for which plot is created
    N_range2 = np.linspace(10, 10**6, 100).astype(int)

    # set up plot visualisation
    fig2 = pl.figure()
    subplot_list=[211,212]

    fig2.add_subplot((subplot_list[0]))
    # plots time average errors for f(x)=x
    timeAV_errorPlot(1.0,V_bistable,Vprime_bistable,0.5,0.005,N_range2,0.005, idendity)
    pl.legend(loc='best')
    pl.xlabel("Time step number N")
    pl.ylabel("Absolute error")
    pl.title("Time average convergence for $f(x)=x$ (above) and $f(x)=x^2$ (below)")

    # plots time average errors for f(x)=x^2
    fig2.add_subplot((subplot_list[1]))
    timeAV_errorPlot(1.0,V_bistable,Vprime_bistable,0.5,0.005,N_range2,0.005, square)
    pl.legend(loc='best')
    pl.xlabel("Time step number N")
    pl.ylabel("Absolute error")    

    print ("Exercise 3 Part 2: The plot shows the evolution of errors of the time \
averages as a function of time step number N on a semilog scaled coordinate system. \
The errors \
of time averages depend linearly on N on this scale such that the dependency on N \
must be exponential, thus the rate of convergence is exponential. Note that the time \
step size dt has been decreased to =0.005. For larger values of dt, one receives a \
falsified convergence rate since the main error for a sufficient large number of \
samples is dominated by the error induced by the time discretization from the Euler-\
Marayuma algorithm.")
    
    pl.show()

