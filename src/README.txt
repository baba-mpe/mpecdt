Please look at: lorenz-mcmc.py, acc_rate_convergence.pdf

In the considered case of an MCMC method for a Lorenz forward model, we receive an 
acceptance rate tending to zero for large sample sizes. Thus, the MCMC algorithm 
tends to reject propositions. 

