import numpy as np
import pylab as pl
from scipy.integrate import quad


"""
File containing code relating to Computational Lab 3 of the Dynamics
MPECDT core course.
"""

def square(x):
    """
    Function to compute the square of the input. If the input is 
    a numpy array, this returns a numpy array of all of the squared values.
    inputs:
    x - value or numpy array of values

    outputs:
    y - value or numpy array containing the square of all of the input values
    """
    return x**2

def excos(x):
    """
    Computes function used for estimation of expectation in exercise 2. If 
    input is numpy array, then also the returned values.
    Inputs:
    x - value or numpy array of values

    Outputs:
    y - value or numpy array containing function values of function used for
    Exercise 2 of all input values
    """
    return np.exp(-10.*x) * np.cos(x)

def convRate(x):
    """
    Computes function that represents a reference convergence for the absolute 
    errors of the Monte-Carlo and Importance Sampling method.
    Inputs:
    x - value or numpy array of values

    Outputs:
    y - value or numpy array containing one over square root of all input
    values
    """
    return x**(-0.5)

def convRatePlot(x):
    """
    Function to plot the reference convergence induced by function convRate for 
    absolute errors of Monte-Carlo and Importance Sampling method on loglog scale
    Inputs:
    x - value or numpy array of values
    """

    pl.loglog(x, convRate(x), 'r', label = r"$\mathdefault{N^{-1/2}}$")
    pl.xlabel(r"$\mathdefault{N}$")
    pl.ylabel(r'$\mathdefault{ N^{-1/2}}$')  

def normal(N):
    """
    Function to return a numpy array containing N iid samples from 
    a N(0,1) distribution.
    
    inputs:
    N - number of samples
    
    outputs:
    X - the samples
    """
    return np.random.randn(N)

def uniform(N):
    """
    Function to return a numpy array containing N iid samples from
    U(0,1) distribution

    Inputs:
    N - number of samples

    Outputs:
    X - samples
    """
    return np.random.uniform(0,1,N)

def transform(N):
    """
    Function to return a numpy array containing N iid samples from
    the distribution induced by density $pi_Y$ from Exercise 2.

    Inputs:
    N - number of samples

    Outputs:
    X - samples
    """
    uniforms = uniform(N)

    # use sampling transformation from Exercise 2
    X = -0.1 * np.log(1.-uniforms + uniforms * np.exp(-10.))

    return X

def normal_pdf(x):
    """
    Function to evaluate the PDF for the normal distribution (the
    normalisation coefficient is ignored). If the input is a numpy
    array, this returns a numpy array with the PDF evaluated at each
    of the values in the array.

    inputs:
    x - a numpy array of input values
    
    outputs:
    rho - a numpy array of rho(x)
    """
    return np.exp(-x**2/2)

def uniform_pdf(x):
    """
    Function to evaluate the PDF for the standard uniform
    distribution. If the input is a numpy array, this returns a numpy
    array of all of the squared values.

    inputs:
    x - a numpy array of input values
    
    outputs:
    rho - a numpy array of rho(x)
    """
    # interpret x as array with dimension at least = 1
    x = np.array(x, ndmin=1)

    y = np.ones(np.shape(x))
    y[x<0] = 0.0
    y[x>1] = 0.0                        

    return y

def transform_pdf(x):
    """
    Function to evaluate the PDF $pi_Y$ from Exercise 2. If the input 
    is a numpy array, this returns a numpy array of all of the squared 
    values.

    inputs:
    x - a numpy array of input values
    
    outputs:
    rho - a numpy array of rho(x)
    """

    # interpret x as array with dimension at least = 1
    x = np.array(x, ndmin=1)

    # set conditions and functions for piecewise definition of $pi_Y$
    conds = [x < 0, (x >= 0) & (x <= 1), x > 1]
    funcs = [lambda x: 0, lambda x: (10. * np.exp(-10.*x))/(1. - np.exp(-10.)), 
            lambda x: 0]

    return np.piecewise(x, conds, funcs)

def importance(f, Y, rho, rhoprime, N):
    """
    Function to compute the importance sampling estimate of the
    expectation E[f(X)], with N samples

    inputs:
    f - a Python function that evaluates a chosen mathematical function on
    each entry in a numpy array
    Y - a Python function that takes N as input and returns
    independent individually distributed random samples from a chosen
    probability distribution
    rho - a Python function that evaluates the PDF for the desired distribution
    on each entry in a numpy array
    rhoprime - a Python function that evaluates the PDF for the
    distribution for Y, on each entry in a numpy array
    N - the number of samples to use
    """
    
    Y = Y(N)
    ratioRange = rho(Y)/rhoprime(Y)
    sampleRange = f(Y) * rho(Y) / rhoprime(Y)
    theta = (np.sum(ratioRange))**(-1) * np.sum( sampleRange )

    return theta


def av_importance(f, Y, rho, rhoprime, N, M):
    """
    Function to compute averages from importance sampling estimates of the
    expectation E[f(X)], with N samples, with M estimates. Not used for assignment,
    but could still be considered at some other point of time.

    Inputs:
    M - number of importance sampling estimates used for average
    for rest, see function importance
    """    
    
    thetaRange = np.zeros(M)
    for i in range(M):
        thetaRange[i] = importance(f, Y, rho, rhoprime, N)
    theta_av = np.mean(thetaRange)
    
    return theta_av


def trueExpec(f, rho):
    """
    Function to compute E[f(X)] based on integrating f multiplied by the pdf rho.
    To be considered as true or reference E[f(X)]. Considered for pdf rho with
    support inside the unity interval.

    Inputs:
    See function importance
    """

    integrand = lambda x: f(x)*rho(x)
    return quad(integrand, 0, 1)     

def IS_errorPlot(f, Y, rho, rhoprime, N_range):
    """
    Function to plot the absolute error of importance sampling estimates of
    E[f(X)] with respect the used sample number. Uses for loop for calculating
    importance sampling estimates with respect to different sample number.

    Inputs:
    N_range - range of samples used for importance sampling
    for rest, see function importance
    """

    #set up range of importance sampling estimates used for plot
    thetaRange = np.zeros(len(N_range))

    for i in range(len(N_range)):
        thetaRange[i] = importance(f, Y, rho, rhoprime, N_range[i])

    errors = np.absolute(thetaRange - trueExpec(f, rho)[0])

    pl.loglog(N_range, errors, 'b', label = 'Importance Sampling')
    pl.xlabel(r"Sample size $\mathdefault{N}$")
    pl.ylabel(r'Absolute Error')  
    pl.title('Importance Sampling Convergence')

def montecarlo(f, X, N):
    """
    Function to compute the Monte-Carlo estimate of the expectation
    E[f(X)], with N samples

    inputs:
    f - a Python function that applies a chosen mathematical function to
    each entry in a numpy array
    X - a Python function that takes N as input and returns
    independent individually distributed random samples from a chosen
    probability distribution
    N - the number of samples to use
    """

    theta = np.sum(f(X(N)))/N

    return theta

def MC_errorPlot(f, X, rho, N_range):
    """
    Function to plot the absolute error of Monte Carlo estimates of
    E[f(X)] with respect the used sample number. Uses for loop for 
    calculating of Monte Carlo estimates with respect to different 
    sample number.

    Inputs:
    f - a Python function that evaluates a chosen mathematical function on
    each entry in a numpy array
    X - a Python function that takes N as input and returns
    independent individually distributed random samples from a chosen
    probability distribution
    rho - a Python function that evaluates the PDF for the desired distribution
    on each entry in a numpy array, only usde for computation of reference 
    E[f(X)]
    N_range - range of samples used for importance sampling
    """
    
    #set up range of importance sampling estimates used for plot
    thetaRange = np.zeros(len(N_range))

    for i in range(len(N_range)):
        thetaRange[i] = montecarlo(f, X, N_range[i])

    errors = np.absolute(thetaRange - trueExpec(f, rho)[0])

    pl.loglog(N_range, errors, 'g', label = 'Monte Carlo')
    pl.xlabel(r"Sample size $\mathdefault{N}$")
    pl.ylabel(r'Absolute Error')  
    pl.title('Convergence of Monte-Carlo')


if __name__ == '__main__':

################# Exercise 1 #########

# Computes single Importance Sampling estimate
    theta = importance(square,normal,
                       uniform_pdf,normal_pdf,1000)
    print "Importance Sampling estimate for Exercise 1 =", theta

# Plots convergence of Importance Sampling
    IS_errorPlot(square,normal,
                       uniform_pdf,normal_pdf,np.logspace(1.0, 6.0, 100))
    
    print ("Exercise 1: The graph shows that the absolute error of the Importance \
Sampling (IS) method has the general tendency to decrease for increasing number \
of samples used. The dependence is on average linear on the loglog scale. \
Beside this general tendency there are fluctuations in the error evolution \
represented by the zigzag shaped graph which reflects the random nature of \
the approximation.")

    pl.show()

################# Exercise 2 #########

# Computes single Importance Sampling estimate
    thetaPrime = importance(excos,transform,
                       uniform_pdf,transform_pdf,10000)
    print "Importance Sampling estimate for Exercise 2 =", thetaPrime

# For exercise 2, one could also compute the analytic value =
# 10./101. - (10. *  np.cos(1.) - np.sin(1.)) / (101. * np.exp(10.))
# of E[f(X)] as true or reference value instead of using function trueExpec.
    
# Request for user input
    response = input("In addition to Importance Sampling convergence, do you want to \
see Monte Carlo convergence and corresponding convergence rate inside plot for comparison? \
Type 'y' for yes and 'n' for no: ") 
    
    if response == 'y':
        print("Exercise 2: As before, the absolute error of IS as well as of Monte Carlo (MC) \
decreases on average by increasing N. One can see that the convergence dependence of the \
error of both procedures on N is on average equal to N^{-0.5}. Due to the random nature of \
the approximations, again they fluctuate so that their graphs are zigzag shaped. MC generally shows \
smaller absolute errors as IS in this example which is the result of unfavourable weighting in the \
summation appearing in IS.")

    # Plots Monte Carlo vs. Importance Sampling convergence plus convergence rate
        IS_errorPlot(excos, transform, uniform_pdf, 
                        transform_pdf, np.logspace(1.0, 6.0, 100))
        MC_errorPlot(excos, uniform, uniform_pdf, 
                        np.logspace(1.0, 6.0, 100))
        convRatePlot(np.logspace(1.0, 6.0, 100))

        pl.xlabel(r"Sample size $\mathdefault{N}$")
        pl.ylabel(r'Absolute Error')  
        pl.title('Convergence of Monte-Carlo vs. Importance Sampling')   
        pl.legend(loc='best')
   
    elif response == 'n':
        print("Exercise 2: As before, the absolute error of IS decreases on average by increasing N. \
The dependence is on average linear on the loglog scale. \
Due to the random nature of the approximation, again the error evolution fluctuates so that the graph \
is zigzag shaped.")
    
    # Plots Importance Sampling convergence
        IS_errorPlot(excos, transform, uniform_pdf, 
                        transform_pdf, np.logspace(1.0, 6.0, 100))

    else:
        raise NameError("Choose 'y' or 'n' as answer to user question.")

    pl.show()
